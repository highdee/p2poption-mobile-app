import 'package:flutter/services.dart';

class AssetServices{
    static Future<String> loadAsset(String path) async{
        return rootBundle.loadString(path);
    }
}