import 'package:flutter/material.dart';

class Messages{
  static void snackSuccess({@required BuildContext context, @required String message}){
    ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
            content: Text(message,
                style: TextStyle(color: Colors.white, fontWeight: FontWeight.w400)
            ),
            backgroundColor: Colors.green,
            duration: Duration(seconds: 2),
        ),
    );
  }
  static void snackError({@required BuildContext context, @required String message}){
    ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
            content: Text(message,
                style: TextStyle(color: Colors.white, fontWeight: FontWeight.w400)
            ),
            backgroundColor: Colors.red,
            duration: Duration(seconds: 2),
        ),

    );
  }
}