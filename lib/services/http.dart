import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as Http;
import 'package:optionp2p/services/utility.dart';
class HttpService{
    static String api_endpoint = "https://tradeadmin.optionp2p.io/api/v1";
    static String bearer_token = "";

    static Future<Http.Response> post({String url, Map payload, BuildContext context}) async{
        Uri endpoint = Uri.parse(url);
        Http.Response response = await Http.post(endpoint, body: payload, headers: {'Accept': 'application/json', 'Authorization':'Bearer '+bearer_token});

        handleHttpError(response, context);
        return response;
    }

    static Future<Http.Response> get({String url, BuildContext context}) async{
        Uri endpoint = Uri.parse(url);
        Http.Response response = await Http.get(endpoint, headers: {'Accept': 'application/json'});

        handleHttpError(response, context);
        return response;
    }

    static void handleHttpError(Http.Response response, BuildContext context){
        switch(response.statusCode){
            case 200:
                var data = jsonDecode(response.body);
                if(data is Map){
                    Map dt = data;
                    if(dt.containsKey('status')){
                        if(!dt['status']){
                            String message = '';
                            if(dt['message'] != null) message = dt['message'];
                            if(dt['msg'] != null) message = dt['msg'];

                            Messages.snackError(context: context, message: message);
                        }
                    }
                }
                break;
            case 401:
                Messages.snackError(context: context, message: "You are not authorized");
                break;
            case 404:
                Messages.snackError(context: context, message: "Request not found");
                break;
            case 422:
                Map resp = jsonDecode(response.body);
                Map errors = resp['errors'];
                String error= errors.entries.first.value[0];
                Messages.snackError(context: context, message: error);
                break;
            case 500:
                Messages.snackError(context: context, message: "Internal server error occured, please try again.");
                break;
            default :
                // Messages.snackError(context: context, message: "Unknown error occured, please contact admin");
                // break;
        }
    }
}