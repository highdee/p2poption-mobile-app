import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:optionp2p/constants/ui.dart';
import 'package:optionp2p/views/widgets/components/loader.dart';

class P2pButton extends StatelessWidget {

    double borderRadius;
    double width;
    double height;
    double fontSize;

    String text;
    Color bg;
    Color textColor;
    Color borderColor;

    bool loading;

    var callback;

    P2pButton({
        Key key,
        this.borderRadius =  4.0,
        this.text =  '',
        this.bg =  UI.tertiary_color_light,
        this.textColor =  UI.secondary_color,
        this.borderColor =  UI.secondary_color,
        this.width = 140,
        this.height = 40,
        this.fontSize = 15,
        this.loading=false,
        this.callback
    }):super(key: key){

    }

  @override
  Widget build(BuildContext context) {
    return InkWell(
        onTap: (){
            if(callback != null){
                callback();
            }
        },
        child: Container(
            padding: EdgeInsets.symmetric(horizontal: 5,vertical: 5),
            width: width,
            height: height,
            child: loading ?
            Center(
                child: P2pLoader(color: this.textColor,),
            ) :
            Center(child: Text(text, style: TextStyle(color: textColor, fontSize: fontSize))),
            decoration: BoxDecoration(
                color: bg,
                border: Border.all(color: borderColor),
                borderRadius: BorderRadius.circular(borderRadius)
            ),
        ),
    );
  }
}
