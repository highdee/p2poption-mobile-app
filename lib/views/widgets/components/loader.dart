import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'dart:io';

import 'package:optionp2p/constants/ui.dart';

class P2pLoader extends StatelessWidget{
    Color color;
    P2pLoader({Key key, this.color=UI.primary_color_light}):super(key : key);

    @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
        width: 20,
        height: 20,
        child:
        // Container(child: CupertinoActivityIndicator(), color: Colors.grey.withOpacity(0.3),) :
        CircularProgressIndicator(valueColor: AlwaysStoppedAnimation<Color>(this.color), strokeWidth: 2),
    );
  }
}