import 'package:flutter/material.dart';
import 'package:optionp2p/views/widgets/appbar/app_bar.dart';

class MyScaffold extends StatefulWidget {

    Widget body;
    MyAppBar appBar;
    Color backgroundColors = null;
    GlobalKey<ScaffoldState> drawerKey = GlobalKey();

    MyScaffold({this.body, this.appBar, this.drawerKey, this.backgroundColors});

  @override
  _MyScaffoldState createState() => _MyScaffoldState();
}

class _MyScaffoldState extends State<MyScaffold> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: widget.appBar,
        body: SafeArea(
            top: false,
          child: Container(
              height: MediaQuery.of(context).size.height,
              width: MediaQuery.of(context).size.width,
              child: SingleChildScrollView(
                  child: widget.body,
              ),
          ),
        ),
        key: widget.drawerKey,
        backgroundColor: widget.backgroundColors ?? Theme.of(context).primaryColor
    );
  }
}
