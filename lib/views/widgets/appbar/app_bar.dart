import 'package:flutter/material.dart';
import 'dart:io';

import 'package:optionp2p/views/widgets/appbar/android_bar.dart';
import 'package:optionp2p/views/widgets/appbar/ios_bar.dart';

class MyAppBar extends StatelessWidget implements PreferredSizeWidget {
    @override
    Size get preferredSize => const Size.fromHeight(60);

    Color backgroundColor;
    String title;
    Widget leading;
    bool showLeading = true ;
    double elevation = 2 ;
    List<Widget> actions;


    MyAppBar({
        this.title,
        this.backgroundColor,
        this.leading,
        this.actions,
        this.showLeading,
        this.elevation
    });

    @override
    Widget build(BuildContext context) {

        return
            Platform.isIOS ?
            IosBar(
                title: Text(title),
                backgroundColor: backgroundColor,
                leading: leading,
                actions: actions,
                showLeading: showLeading,
                elevation:elevation
            ):
            DriodBar(
                title: Text(title),
                backgroundColor: backgroundColor,
                leading: leading,
                actions: actions,
                showLeading: showLeading,
                elevation: elevation,
            );
    }
}