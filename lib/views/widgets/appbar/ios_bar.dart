import 'package:flutter/material.dart';

class IosBar extends StatelessWidget {

    Color backgroundColor;
    Widget title;
    Widget leading;
    bool showLeading = true ;
    double elevation = 2 ;
    bool center_title = true ;
    List<Widget> actions;


    IosBar({
        this.title,
        this.backgroundColor,
        this.leading,
        this.actions,
        this.showLeading,
        this.elevation
    });

    @override
    Widget build(BuildContext context) {

        return AppBar(
            title: title,
            backgroundColor: backgroundColor,
            centerTitle: center_title,
            leading: showLeading != null ?  IconButton(icon: Icon(Icons.arrow_back_ios), onPressed: ()=> Navigator.pop(context)) : null,
            actions: actions,
            elevation: elevation,
        );
    }
}