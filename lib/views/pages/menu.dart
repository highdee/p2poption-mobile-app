import 'dart:ffi';
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:optionp2p/constants/ui.dart';
import 'package:optionp2p/store/main-state.dart';
import 'package:provider/provider.dart';

class Menu extends StatefulWidget {
  var navigateViewTo;

  Menu({Key key, this.navigateViewTo}) : super(key: key);


  @override
  _MenuState createState() => _MenuState();
}

class _MenuState extends State<Menu> {
  List<Map> menu = [
    {
      'title':'Live Trade',
      'icon':Icons.bar_chart,
      'icon_color':Colors.green,
      'url':'dashboard/trade-room',
      'auth_required':true,
      'type':'web'
    },
    {
      'title':'Demo Trade',
      'icon':Icons.bar_chart,
      'icon_color':Colors.red,
      'url':'demo-trade',
      'type':'web'
    },
    {
      'title':'Trading History',
      'icon':Icons.history,
      'auth_required':true,
      'url':'dashboard/trade-room?history=',
      'type':'web'
    },
    {
      'title':'Deposit/Withdrawal',
      'icon':Icons.attach_money,
      'auth_required':true,
      'url':'dashboard/finance/deposit',
      'type':'web'
    },
    {
      'title':'Profile',
      'icon':Icons.switch_account,
      'auth_required':true,
      'url':'dashboard/profile/details',
      'type':'web'
    },
    {
      'title':'Support',
      'icon':Icons.chat,
      'url':'dashboard/trade-room?support=',
      'type':'web'
    },
    {
      'title':'Sign in',
      'icon':Icons.login,
      'url':'login',
      'type':'app',
      'category':'loggedout'
    },
    {
      'title':'Logout',
      'icon':Icons.login,
      'url':'demo-trade',
      'type':'web',
      'category':'loggedin'
    },
  ];

  MainState state;

  void initState(){
    state = Provider.of<MainState>(context, listen: false);
    
    if(state.user != null){
      setState(() {
        menu = menu.where((element) => element['category'] != 'loggedout').toList();
        menu = menu.where((element) => element['title'] != 'Demo Trade').toList();
      });
    }else{
      setState(() {
        menu = menu.where((element) => element['category'] != 'loggedin').toList();
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: UI.web_primary.withOpacity(1)
      ),
      width: MediaQuery.of(context).size.width,
      child: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [ 
            Container(
              height: MediaQuery.of(context).size.height,
              margin: EdgeInsets.symmetric(horizontal: 20, vertical: 0),
              child: GridView.builder(
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                      childAspectRatio: 3/2.3,
                      crossAxisCount: 2,
                      mainAxisExtent: 200,
                      crossAxisSpacing: 10,
                      mainAxisSpacing: 10
                  ),
                  shrinkWrap: true,
                  itemCount: menu.length,
                  itemBuilder: (context, index){
                    Map option = menu[index];
                    return InkWell(
                      child: Container(
                        decoration: BoxDecoration(
                          border: Border.all(
                            color: Colors.white12
                          ),
                          borderRadius: BorderRadius.circular(10)
                        ),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Container(
                              child: Icon(option['icon'], size: 35, color: option['icon_color'] ?? Colors.white70),
                              decoration: BoxDecoration(
                                color: UI.web_primary_off.withOpacity(1),
                                borderRadius: BorderRadius.circular(100)
                              ),
                              padding: EdgeInsets.symmetric(vertical: 30, horizontal: 30),
                            ),
                            Container(
                              margin: EdgeInsets.only(top: 10),
                              child: Text(
                                option['title'].toString().toUpperCase(),
                                textAlign: TextAlign.center,
                                style: TextStyle(fontSize: 17, color: Colors.white70),
                              ),
                            )
                          ],
                        ),
                      ),
                      onTap: (){
                        if(option['type'] == 'web'){
                          if(option['title'] == 'Logout'){
                            logout();
                          }
                          if(option['auth_required'] != null){
                              if(state.user == null){
                                Navigator.pushNamed(context, 'login');
                                return false;
                              }
                          }
                          widget.navigateViewTo(option['url']);
                        }
                        else{
                          Navigator.pushNamed(context, option['url']);
                        }
                      },
                    );
                  }
              ),
            ),
          ],
        ),
      )
    );
  }

  void logout(){
    state.sharedPref.remove('user');
    state.user = null;
  }
}
