import 'package:flutter/material.dart';
import 'package:optionp2p/constants/ui.dart';
import 'package:optionp2p/views/widgets/appbar/app_bar.dart';
import 'package:optionp2p/views/widgets/layout/scaffold.dart';

class TradingProfilePage extends StatefulWidget {
  @override
  _TradingProfilePageState createState() => _TradingProfilePageState();
}

class _TradingProfilePageState extends State<TradingProfilePage> {
  @override
  Widget build(BuildContext context) {
    return MyScaffold(
      appBar: MyAppBar(
        title: "TRADING PROFILE",
      ),
      body: Container(
        padding: EdgeInsets.symmetric(vertical: 20, horizontal: 20),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    width: UI.getWidthMediaQ(context) * 0.25,
                    height: UI.getWidthMediaQ(context) * 0.25,
                    decoration: BoxDecoration(
                      color: UI.secondary_color_dark,
                      borderRadius: BorderRadius.circular(100)
                    ),
                    child: Icon(Icons.account_circle_rounded, size: UI.getWidthMediaQ(context)*0.24),
                  )
                ],
              )
            ),
            Container(
              margin: EdgeInsets.only(top: 10),
              child: Text('Idowu Aladesiun', style: TextStyle(fontSize: 19),)
            ),
            Container(
                margin: EdgeInsets.only(top: 7),
                child: Text('highdee.ai@gmail.com', style: TextStyle(fontSize: 11, color: UI.tertiary_color),)
            ),
            Container(
              decoration: BoxDecoration(
                color: UI.primary_color_light
              ),
              margin: EdgeInsets.only(top: 20),
              padding: EdgeInsets.symmetric(vertical: 13),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Container(
                    child: Row(
                      children: [
                        Icon(Icons.account_circle_rounded, size: 17,),
                        Text(' ID: 64F4FG', style: TextStyle(color: Colors.white, fontSize: 13),)
                      ],
                    ),
                  ),
                  Container(
                    child: Row(
                      children: [
                        Icon(Icons.language, size: 17),
                        Text(' 192.32.122.2', style: TextStyle(color: Colors.white, fontSize: 13),)
                      ],
                    ),
                  ),
                ],
              ),
            ),
            Container(
              decoration: BoxDecoration(
                  color: UI.primary_color_light
              ),
              margin: EdgeInsets.only(top: 20),
              padding: EdgeInsets.symmetric(vertical: 0, horizontal: 15),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text('Live account statistics for today:'),
                  IconButton(
                      onPressed: (){},
                      icon: Icon(Icons.refresh, size: 20,)
                  )
                ],
              ),
            ),
            Container(
              decoration: BoxDecoration(
                  border: Border(
                      left: BorderSide(color: UI.primary_color_light),
                      right: BorderSide(color: UI.primary_color_light)
                  )
              ),
              child: Column(
                children: [
                  Container(
                    padding: EdgeInsets.symmetric(vertical: 13, horizontal: 15),
                    decoration: BoxDecoration(
                      border: Border(bottom: BorderSide(color: UI.primary_color_light))
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text('Trading Turnover'),
                        Text('\$50,000', style: TextStyle(fontSize: 18, color: UI.tertiary_color),),
                      ],
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(vertical: 13, horizontal: 15),
                    decoration: BoxDecoration(
                      border: Border(bottom: BorderSide(color: UI.primary_color_light))
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text('Deal'),
                        Text('\$50,000', style: TextStyle(fontSize: 18, color: UI.tertiary_color),),
                      ],
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(vertical: 13, horizontal: 15),
                    decoration: BoxDecoration(
                      border: Border(bottom: BorderSide(color: UI.primary_color_light))
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text('Net Turnover'),
                        Text('\$50,000', style: TextStyle(fontSize: 18, color: UI.tertiary_color),),
                      ],
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(vertical: 13, horizontal: 15),
                    decoration: BoxDecoration(
                      border: Border(bottom: BorderSide(color: UI.primary_color_light))
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text('Trading Profit'),
                        Text('\$2,000', style: TextStyle(fontSize: 18, color: UI.tertiary_color),),
                      ],
                    ),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
