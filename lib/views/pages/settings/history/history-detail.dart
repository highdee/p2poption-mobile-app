import 'package:flutter/material.dart';
import 'package:optionp2p/constants/ui.dart';
import 'package:optionp2p/views/widgets/appbar/app_bar.dart';
import 'package:optionp2p/views/widgets/layout/scaffold.dart';

class HistoryDetail extends StatefulWidget {
  @override
  _HistoryDetailState createState() => _HistoryDetailState();
}

class _HistoryDetailState extends State<HistoryDetail> {
  @override
  Widget build(BuildContext context) {
    return MyScaffold(
        appBar: MyAppBar(
            title: 'HISTORY DETAIL',
            backgroundColor: UI.secondary_color_dark,
            elevation: 0,
        ),
        body: Container(
          child: Column(
              children: [
                  Container(
                      decoration: BoxDecoration(
                          color: UI.secondary_color_dark
                      ),
                      padding: EdgeInsets.symmetric(vertical: 60),
                      child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                              Flexible(
                                  flex: 1,
                                  child: Column(
                                      children: [
                                          Text('BTC/USDT', style: TextStyle(fontSize: 18, fontWeight: FontWeight.w700)),
                                          Container(
                                              child: Image.asset('assets/images/win.png'),
                                              margin: EdgeInsets.symmetric(vertical: 10),
                                          ),
                                          Text('YOU WON!', style: TextStyle(fontSize: 17, fontWeight: FontWeight.w700)),
                                      ],
                                  ),
                              ),
                              Flexible(
                                  flex: 1,
                                  child: Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                          Text('Investment', style: TextStyle(fontSize: 12)),
                                          Container(
                                              margin: EdgeInsets.only(top: 5, bottom: 15),
                                              child: Text('\$9,43.00', style: TextStyle(fontSize: 20, color: Colors.greenAccent)),
                                          ),
                                          Text('Result (P/L)', style: TextStyle(fontSize: 12)),
                                          Container(
                                              margin: EdgeInsets.only(top: 5, bottom: 10),
                                              child: Text('\$9,43.00', style: TextStyle(fontSize: 20, color: Colors.greenAccent)),
                                          ),
                                          Text('Result (%)', style: TextStyle(fontSize: 12)),
                                          Container(
                                              margin: EdgeInsets.only(top: 5, bottom: 10),
                                              child: Text('10%', style: TextStyle(fontSize: 20, color: Colors.greenAccent)),
                                          ),
                                      ],
                                  ),
                              ),
                          ],
                      )
                  ),
                  Container(
                      padding: EdgeInsets.symmetric(vertical: 60),
                      child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                              Flexible(
                                  flex: 1,
                                  child: Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                          Text('Purchase Time', style: TextStyle(fontSize: 12, color: UI.tertiary_color)),
                                          Container(
                                              margin: EdgeInsets.only(top: 6, bottom: 20),
                                              child: Text('03-Nov - 15:17:31', style: TextStyle(fontSize: 16, color: Colors.white)),
                                          ),
                                          Text('Opening Price', style: TextStyle(fontSize: 12, color: UI.tertiary_color)),
                                          Container(
                                              margin: EdgeInsets.only(top: 6, bottom: 20),
                                              child: Text('14387.00', style: TextStyle(fontSize: 16, color: Colors.white)),
                                          ),
                                          Text('Strike Price', style: TextStyle(fontSize: 12, color: UI.tertiary_color)),
                                          Container(
                                              margin: EdgeInsets.only(top: 6, bottom: 20),
                                              child: Text('13487.00', style: TextStyle(fontSize: 16, color: Colors.white)),
                                          ),
                                      ],
                                  ),
                              ),
                              Container(
                                  decoration: BoxDecoration(
                                      color: UI.secondary_color_dark
                                  ),
                                  width: 1,
                                  height: 200,
                              ),
                              Flexible(
                                  flex: 1,
                                  child: Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                          Text('Purchase Time', style: TextStyle(fontSize: 12, color: UI.tertiary_color)),
                                          Container(
                                              margin: EdgeInsets.only(top: 6, bottom: 20),
                                              child: Text('03-Nov - 15:17:31', style: TextStyle(fontSize: 16, color: Colors.white)),
                                          ),
                                          Text('Opening Price', style: TextStyle(fontSize: 12, color: UI.tertiary_color)),
                                          Container(
                                              margin: EdgeInsets.only(top: 6, bottom: 20),
                                              child: Text('14387.00', style: TextStyle(fontSize: 16, color: Colors.white)),
                                          ),
                                          Text('Strike Price', style: TextStyle(fontSize: 12, color: UI.tertiary_color)),
                                          Container(
                                              margin: EdgeInsets.only(top: 6, bottom: 20),
                                              child: Text('13487.00', style: TextStyle(fontSize: 16, color: Colors.white)),
                                          ),
                                      ],
                                  ),
                              ),
                          ],
                      )
                  )
              ],
          ),
        ),
    );
  }
}
