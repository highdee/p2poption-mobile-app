import 'package:flutter/material.dart';
import 'package:optionp2p/constants/ui.dart';
import 'package:optionp2p/views/pages/settings/history/history-detail.dart';
import 'package:optionp2p/views/widgets/appbar/app_bar.dart';
import 'package:optionp2p/views/widgets/layout/scaffold.dart';
class TradingHistories extends StatefulWidget {
  @override
  _TradingHistoriesState createState() => _TradingHistoriesState();
}

class _TradingHistoriesState extends State<TradingHistories> {
  @override
  Widget build(BuildContext context) {
    return MyScaffold(
        appBar: MyAppBar(
            title: 'TRADING HISTORY',
        ),
        body: Container(
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            padding: EdgeInsets.symmetric(horizontal: 10),
            child: ListView.builder(
                itemCount: 20,
                itemBuilder: (BuildContext context, item){
                return Container(
                    margin: EdgeInsets.symmetric(vertical: 5),
                    width: MediaQuery.of(context).size.width,
                  child: InkWell(
                      child: Container(
                          decoration: BoxDecoration(
                              color: UI.primary_color_off
                          ),
                        child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                                Container(
                                    padding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                                    child: Row(
                                        children: [
                                            Container(
                                                child: Image.asset('assets/images/win.png', width: 30, height: 30),
                                                margin: EdgeInsets.only(right: 10),
                                            ),
                                            Column(
                                                children: [
                                                    Container(
                                                        child: Text('Bitcoin', style: TextStyle(fontSize: 12,color: Colors.white, fontWeight: FontWeight.w600)),
                                                        margin: EdgeInsets.only(bottom: 4),
                                                    ),
                                                    Text('-1.51%', style: TextStyle(fontSize: 11,color: Colors.red))
                                                ],
                                            )
                                        ],
                                    ),
                                ),
                                Container(
                                    child: Text('Dec:03|06:19', style: TextStyle(color: Colors.white, fontSize: 11)),
                                ),
                                Container(
                                    child: Text('-\$345.80', style: TextStyle(color: Colors.red, fontSize: 11)),
                                ),
                                Container(
                                    child: Text('-\$345.80', style: TextStyle(color: Colors.white, fontSize: 11)),
                                ),
                            ],
                        ),
                      ),
                      onTap: (){
                          Navigator.push(context, MaterialPageRoute(builder: (BuildContext context)=> HistoryDetail()));
                      },
                  ),
                );
            }),
        ),
    );
  }
}
