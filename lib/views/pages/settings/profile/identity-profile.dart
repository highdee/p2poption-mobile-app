import 'package:flutter/material.dart';
import 'package:optionp2p/constants/ui.dart';
import 'package:optionp2p/store/main-state.dart';
import 'package:provider/provider.dart';

class IdentityProfile extends StatefulWidget {
    var callback;
    IdentityProfile({this.callback});
  @override
  _IdentityProfileState createState() => _IdentityProfileState();
}

class _IdentityProfileState extends State<IdentityProfile> {
  MainState state;

  TextEditingController firstnameController = TextEditingController();
  TextEditingController lastnameController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController phoneController = TextEditingController();
  Map<String, dynamic> select_values = {};

  void initState(){
      state = Provider.of<MainState>(context, listen: false);


      firstnameController.text = state.user.firstname;
      lastnameController.text = state.user.lastname;
      emailController.text = state.user.email;
      phoneController.text = state.user.phone;
      select_values['gender'] = state.user.gender;

      Future.delayed(Duration(seconds: 1), (){
          widget.callback(computeData());
      });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: EdgeInsets.symmetric(horizontal: 20),
      child: Column(
          children: [
              Container(
                  padding: EdgeInsets.symmetric(vertical: 15, horizontal: 12),
                  margin: EdgeInsets.only(bottom: 10),
                  width: MediaQuery.of(context).size.width,
                  decoration: BoxDecoration(
                      color: UI.primary_color_light
                  ),
                  child: Text("Identity Info", style: TextStyle(color: Colors.grey.shade200, fontWeight: FontWeight.w600)),
              ),
              Container(
                  padding: EdgeInsets.only(top: 5, bottom: 5, left: 10),
                  width: MediaQuery.of(context).size.width,
                  // height: 50,
                  child: Table(
                      columnWidths: {
                          0 : FlexColumnWidth(1),
                          1 : FlexColumnWidth(3),
                      },
                      children: [
                          ProfileInputRow('First Name', firstnameController, required: true),
                          ProfileInputRow('Last Name', lastnameController, required: true),
                          ProfileEmailRow('Email', emailController, required: true),
                          ProfileInputRow('Phone Number', phoneController),
                          // ProfileInputRow('Date of Birth', dobController),
                          ProfileSelectRow('Gender', 'gender'),
                      ],
                  ),
              ),
          ],
      ),
    );
  }
  
  TableRow ProfileInputRow(label, TextEditingController controller, {bool required = false}){
      return TableRow(
          children:[
              TableCell(
                  child: Row(
                      children: [
                          Text(label, style: TextStyle(fontSize: 11, color: UI.tertiary_color)),
                          Container(
                              child: Text(required ? '*':'', style: TextStyle(fontSize: 11, color: UI.tertiary_color_red)),
                              margin: EdgeInsets.only(left: 6),
                          )
                      ],
                  ),
                  verticalAlignment: TableCellVerticalAlignment.middle,
              ),
              Container(
                  margin: EdgeInsets.only(left: 20, bottom: 12),
                  height: 35,
                  child: TextFormField(
                      decoration: InputDecoration(
                          enabledBorder: UnderlineInputBorder(
                              borderSide:BorderSide(width: 0),
                              borderRadius: BorderRadius.circular(1),
                          ),
                          focusedBorder: UnderlineInputBorder(
                              borderSide:BorderSide(width: 0),
                              borderRadius: BorderRadius.circular(1),
                          ),
                          filled: true,
                          fillColor: UI.primary_color_light,
                          hintText: 'Empty',
                          hintStyle: TextStyle(fontSize: 12, color: UI.tertiary_color_dark.withOpacity(0.4)),
                          contentPadding: EdgeInsets.only(bottom: 13,left: 20)
                      ),
                      controller: controller,
                      onChanged: (v){
                          widget.callback(computeData());
                      },
                      style: TextStyle(fontSize: 12, color: UI.tertiary_color_dark),

                  ),
              )
          ],
      );
  }

  TableRow ProfileEmailRow(label, TextEditingController controller, {bool required = false}){
      return TableRow(
          children:[
              TableCell(
                  child: Row(
                      children: [
                          Text(label, style: TextStyle(fontSize: 11, color: UI.tertiary_color)),
                          Container(
                              child: Text(required ? '*':'', style: TextStyle(fontSize: 11, color: UI.tertiary_color_red)),
                              margin: EdgeInsets.only(left: 6),
                          )
                      ],
                  ),
                  verticalAlignment: TableCellVerticalAlignment.middle,
              ),
              TableCell(
                  child: Row(
                      children: [
                          Flexible(child: Container(
                              margin: EdgeInsets.only(left: 20, bottom: 12),
                              height: 35,
                              child: TextFormField(
                                  decoration: InputDecoration(
                                      enabledBorder: UnderlineInputBorder(
                                          borderSide:BorderSide(width: 0),
                                          borderRadius: BorderRadius.circular(1),
                                      ),
                                      focusedBorder: UnderlineInputBorder(
                                          borderSide:BorderSide(width: 0),
                                          borderRadius: BorderRadius.circular(1),
                                      ),
                                      filled: true,
                                      fillColor: UI.primary_color_light,
                                      hintText: 'Empty',
                                      hintStyle: TextStyle(fontSize: 12, color: UI.tertiary_color_dark.withOpacity(0.4)),
                                      contentPadding: EdgeInsets.only(bottom: 13,left: 20)
                                  ),
                                  controller: controller,
                                  style: TextStyle(fontSize: 12, color: UI.tertiary_color_dark),
                                  enabled: false,
                              ),
                          )),
                          Container(
                              child: Row(
                                  children: [
                                      Container(
                                          child: Icon(Icons.close, color: Colors.orangeAccent, size: 12),
                                          margin: EdgeInsets.only(right: 2,left: 15),
                                      ),
                                      Visibility(
                                          visible: state.user.email_verified_at != null,
                                          child: Text('verified', style: TextStyle(color: Colors.greenAccent, fontSize: 10),)
                                      ),
                                      Visibility(
                                          visible: state.user.email_verified_at == null,
                                          child: Text('unverified', style: TextStyle(color: Colors.orangeAccent, fontSize: 10),)
                                      )
                                  ],
                              ),
                          )
                      ],
                  )
              )
          ],
      );
  }

  TableRow ProfileSelectRow(label, String value, {bool required = false}){
      return TableRow(
          children:[
              TableCell(
                  child: Row(
                      children: [
                          Text(label, style: TextStyle(fontSize: 11, color: UI.tertiary_color)),
                          Container(
                              child: Text(required ? '*':'', style: TextStyle(fontSize: 11, color: UI.tertiary_color_red)),
                              margin: EdgeInsets.only(left: 6),
                          )
                      ],
                  ),
                  verticalAlignment: TableCellVerticalAlignment.middle,
              ),
              Container(
                  margin: EdgeInsets.only(left: 20, bottom: 12),
                  height: 35,
                  constraints: BoxConstraints(
                      maxWidth: 50
                  ),
                  decoration: BoxDecoration(
                      color: UI.primary_color_light
                  ),
                  child: DropdownButton(
                      items: [
                          DropdownMenuItem(child: Text('Male', style: TextStyle(color: UI.tertiary_color_dark, fontSize: 13)), value: 'male'),
                          DropdownMenuItem(child: Text('Female', style: TextStyle(color: UI.tertiary_color_dark, fontSize: 13)), value: 'female'),
                      ],
                      onChanged: (v){
                          setState(() {select_values[value] = v;});
                          widget.callback(computeData());
                      },
                      hint: Container(
                          child: Text('Empty', style: TextStyle(fontSize: 12, color: UI.tertiary_color_dark)),
                          padding: EdgeInsets.symmetric(horizontal: 20),
                      ),
                      dropdownColor: UI.primary_color_light,
                      underline: Container(color: Colors.transparent,),
                      isExpanded: true,
                      value: select_values[value],
                  ),
              )
          ],
      );
  }

  computeData(){
      return {
          'firstname' : firstnameController.text,
          'lastname' : lastnameController.text,
          'email' : emailController.text,
          'phone' : phoneController.text,
          ...select_values
      };
  }
}
