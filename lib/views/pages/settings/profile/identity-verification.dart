import 'package:flutter/material.dart';
import 'package:optionp2p/constants/ui.dart';
import 'package:optionp2p/views/widgets/appbar/app_bar.dart';
import 'package:optionp2p/views/widgets/layout/scaffold.dart';

class IdentityVerification extends StatefulWidget {
  @override
  _IdentityVerificationState createState() => _IdentityVerificationState();
}

class _IdentityVerificationState extends State<IdentityVerification> {
  @override
  Widget build(BuildContext context) {
    return MyScaffold(
        appBar: MyAppBar(
            title: 'IDENTITY VERIFICATION',
        ),
        body: Container(
          padding: EdgeInsets.symmetric(horizontal: 15),
          child: Column(
              children: [
                  Container(
                      decoration: BoxDecoration(
                          border: Border.all(color: UI.primary_color_light, width: 1,),
                          borderRadius: BorderRadius.circular(2)
                      ),
                    child: Column(
                        children: [
                            Container(
                                padding: EdgeInsets.symmetric(vertical: 12, horizontal: 12),
                                margin: EdgeInsets.only(bottom: 10),
                                width: MediaQuery.of(context).size.width,
                                decoration: BoxDecoration(
                                    color: UI.primary_color_light,
                                    borderRadius: BorderRadius.circular(2)
                                ),
                                child: Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: [
                                        Text("Identity Status", style: TextStyle(color: Colors.grey.shade300, fontWeight: FontWeight.w600, fontSize: 13)),
                                        Text("Unverified", style: TextStyle(color: Colors.orangeAccent, fontWeight: FontWeight.w600, fontSize: 12))
                                    ],
                                ),
                            ),
                            Container(
                                margin: EdgeInsets.symmetric(vertical: 20),
                                child: MaterialButton(
                                    child: Row(
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        children: [
                                            Container(
                                                child: Text('Upload a valid ID', style: TextStyle(color: Colors.grey)),
                                                margin: EdgeInsets.only(right: 20),
                                            ),
                                            Icon(Icons.add, size: 15, color: Colors.grey)
                                        ],
                                    ),
                                    onPressed: (){},
                                ),
                            )
                        ],
                    ),
                  ),

              ],
          ),
        ),
    );
  }
}
