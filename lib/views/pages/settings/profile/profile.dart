import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:optionp2p/constants/ui.dart';
import 'package:optionp2p/services/utility.dart';
import 'package:optionp2p/store/main-state.dart';
import 'package:optionp2p/views/pages/settings/profile/address-profile.dart';
import 'package:optionp2p/views/pages/settings/profile/identity-profile.dart';
import 'package:optionp2p/views/widgets/appbar/app_bar.dart';
import 'package:optionp2p/views/widgets/components/buttons.dart';
import 'package:optionp2p/views/widgets/layout/scaffold.dart';
import 'package:http/http.dart' as Http;
import 'package:optionp2p/services/http.dart';
import 'package:provider/provider.dart';


class ProfilePage extends StatefulWidget {
  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  bool loading = false;
  Map payload = {};
  MainState state;

  void initState(){
    super.initState();

    state = Provider.of<MainState>(context, listen: false);
  }

  @override
  Widget build(BuildContext context) {
    return MyScaffold(
        appBar: MyAppBar(
            title: 'PROFILE',
        ),
        body: Container(
            padding: EdgeInsets.only(top: 30, bottom: 40),
          child: Column(
              children: [
                  IdentityProfile(callback: getData),
                  AddressProfile(),
                  Container(
                    child: P2pButton(
                      text: 'Update',
                      loading: loading,
                      callback: updateProfile,
                    ),
                    margin: EdgeInsets.only(top: 10),
                  )
              ],
          ),
        ),
    );
  }

  getData(val){
    // print(val);
    setState(() {
      payload = {...payload, ...val};
    });
  }

  updateProfile() async{
    if(loading) return false;

    setState(()=> loading = true);

    payload.removeWhere((key, value) => value == null);
    print(payload);
    Http.Response response = await HttpService.post(
        url:HttpService.api_endpoint+"/update-profile",
        payload:payload,
        context: context);

    if(response.statusCode == 200){
      Map data = jsonDecode(response.body);
      Map user = data['data'];
      user['token'] = state.user.token;
      state.setUser(user);
      Messages.snackSuccess(context: context, message: "Profile updated successfully");
    }
    setState(()=> loading = false);
  }
}
