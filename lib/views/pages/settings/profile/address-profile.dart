import 'package:flutter/material.dart';
import 'package:optionp2p/constants/ui.dart';
import 'package:optionp2p/store/main-state.dart';
import 'package:provider/provider.dart';
class AddressProfile extends StatefulWidget {
    var callback;
    AddressProfile({this.callback});

    @override
    _AddressProfileState createState() => _AddressProfileState();
}

class _AddressProfileState extends State<AddressProfile> {
    String dropdown_select = '';

    MainState state;

    TextEditingController firstnameController = TextEditingController();
    TextEditingController lastnameController = TextEditingController();
    TextEditingController emailController = TextEditingController();
    TextEditingController phoneController = TextEditingController();
    Map<String, dynamic> select_values = {};

    void initState(){
        state = Provider.of<MainState>(context, listen: false);


        firstnameController.text = state.user.firstname;
        lastnameController.text = state.user.lastname;
        emailController.text = state.user.email;
        phoneController.text = state.user.phone;
        select_values['gender'] = state.user.gender;

        Future.delayed(Duration(seconds: 1), (){
            widget.callback(computeData());
        });
    }

    @override
    Widget build(BuildContext context) {
        return Container(
            padding: EdgeInsets.symmetric(horizontal: 20),
            child: Column(
                children: [
                    Container(
                        padding: EdgeInsets.symmetric(vertical: 15, horizontal: 12),
                        margin: EdgeInsets.only(bottom: 10),
                        width: MediaQuery.of(context).size.width,
                        decoration: BoxDecoration(
                            color: UI.primary_color_light
                        ),
                        child: Text("Address Info", style: TextStyle(color: Colors.grey.shade200, fontWeight: FontWeight.w600)),
                    ),
                    Container(
                        padding: EdgeInsets.only(top: 5, bottom: 5, left: 10),
                        width: MediaQuery.of(context).size.width,
                        // height: 50,
                        child: Table(
                            columnWidths: {
                                0 : FlexColumnWidth(1),
                                1 : FlexColumnWidth(3),
                            },
                            children: [
                                ProfileInputRow('Country', required: true),
                                ProfileSelectRow('State/Region'),
                                ProfileSelectRow('City'),
                                ProfileInputRow('Address'),
                                ProfileInputRow('Address Line 2'),
                                ProfileInputRow('Zip code', required: true),
                            ],
                        ),
                    ),
                ],
            ),
        );
    }

    TableRow ProfileInputRow(label, {bool required = false}){
        return TableRow(
            children:[
                TableCell(
                    child: Row(
                        children: [
                            Text(label, style: TextStyle(fontSize: 11, color: UI.tertiary_color)),
                            Container(
                                child: Text(required ? '*':'', style: TextStyle(fontSize: 11, color: UI.tertiary_color_red)),
                                margin: EdgeInsets.only(left: 6),
                            )
                        ],
                    ),
                    verticalAlignment: TableCellVerticalAlignment.middle,
                ),
                Container(
                    margin: EdgeInsets.only(left: 20, bottom: 12),
                    height: 35,
                    child: TextFormField(
                        decoration: InputDecoration(
                            enabledBorder: UnderlineInputBorder(
                                borderSide:BorderSide(width: 0),
                                borderRadius: BorderRadius.circular(1),
                            ),
                            focusedBorder: UnderlineInputBorder(
                                borderSide:BorderSide(width: 0),
                                borderRadius: BorderRadius.circular(1),
                            ),
                            filled: true,
                            fillColor: UI.primary_color_light,
                            hintText: 'Empty',
                            hintStyle: TextStyle(fontSize: 12, color: UI.tertiary_color_dark),
                            contentPadding: EdgeInsets.only(bottom: 13,left: 20)
                        ),
                        style: TextStyle(fontSize: 12, color: UI.tertiary_color_dark),

                    ),
                )
            ],
        );
    }

    TableRow ProfileEmailRow(label, {bool required = false}){
        return TableRow(
            children:[
                TableCell(
                    child: Row(
                        children: [
                            Text(label, style: TextStyle(fontSize: 11, color: UI.tertiary_color)),
                            Container(
                                child: Text(required ? '*':'', style: TextStyle(fontSize: 11, color: UI.tertiary_color_red)),
                                margin: EdgeInsets.only(left: 6),
                            )
                        ],
                    ),
                    verticalAlignment: TableCellVerticalAlignment.middle,
                ),
                TableCell(
                    child: Row(
                        children: [
                            Flexible(child: Container(
                                margin: EdgeInsets.only(left: 20, bottom: 12),
                                height: 35,
                                child: TextFormField(
                                    decoration: InputDecoration(
                                        enabledBorder: UnderlineInputBorder(
                                            borderSide:BorderSide(width: 0),
                                            borderRadius: BorderRadius.circular(1),
                                        ),
                                        focusedBorder: UnderlineInputBorder(
                                            borderSide:BorderSide(width: 0),
                                            borderRadius: BorderRadius.circular(1),
                                        ),
                                        filled: true,
                                        fillColor: UI.primary_color_light,
                                        hintText: 'Empty',
                                        hintStyle: TextStyle(fontSize: 12, color: UI.tertiary_color_dark),
                                        contentPadding: EdgeInsets.only(bottom: 13,left: 20)
                                    ),
                                    style: TextStyle(fontSize: 12, color: UI.tertiary_color_dark),

                                ),
                            )),
                            Container(
                                child: Row(
                                    children: [
                                        Container(
                                            child: Icon(Icons.close, color: Colors.orangeAccent, size: 12),
                                            margin: EdgeInsets.only(right: 2,left: 15),
                                        ),
                                        Text('unverified', style: TextStyle(color: Colors.orangeAccent, fontSize: 10),)
                                    ],
                                ),
                            )
                        ],
                    )
                )
            ],
        );
    }

    TableRow ProfileSelectRow(label, {bool required = false}){
        return TableRow(
            children:[
                TableCell(
                    child: Row(
                        children: [
                            Text(label, style: TextStyle(fontSize: 11, color: UI.tertiary_color)),
                            Container(
                                child: Text(required ? '*':'', style: TextStyle(fontSize: 11, color: UI.tertiary_color_red)),
                                margin: EdgeInsets.only(left: 6),
                            )
                        ],
                    ),
                    verticalAlignment: TableCellVerticalAlignment.middle,
                ),
                Container(
                    margin: EdgeInsets.only(left: 20, bottom: 12),
                    height: 35,
                    constraints: BoxConstraints(
                        maxWidth: 50
                    ),
                    decoration: BoxDecoration(
                        color: UI.primary_color_light
                    ),
                    child: DropdownButtonFormField(
                        items: [
                            DropdownMenuItem(child: Text(''), value: ''),
                            DropdownMenuItem(child: Text('Male'), value: 'male'),
                            DropdownMenuItem(child: Text('Female'), value: 'female')
                        ],
                        onChanged: (v){},
                        hint: Container(
                            child: Text('Empty', style: TextStyle(fontSize: 12, color: UI.tertiary_color_dark)),
                            padding: EdgeInsets.symmetric(horizontal: 20),
                        ),
                        dropdownColor: Colors.red,
                        isExpanded: true,
                        value: dropdown_select,
                        decoration: InputDecoration(
                            enabledBorder: OutlineInputBorder(
                                borderSide: BorderSide.none
                            )
                        ),
                    ),
                )
            ],
        );
    }

    computeData(){
        return {
            'firstname' : firstnameController.text,
            'lastname' : lastnameController.text,
            'email' : emailController.text,
            'phone' : phoneController.text,
            ...select_values
        };
    }
}
