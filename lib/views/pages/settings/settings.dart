import 'package:flutter/material.dart';
import 'package:optionp2p/config/settings.dart';
import 'package:optionp2p/constants/ui.dart';
import 'package:optionp2p/views/widgets/appbar/app_bar.dart';
import 'package:optionp2p/views/widgets/layout/scaffold.dart';

class SettingsPage extends StatefulWidget {
  @override
  _SettingsPageState createState() => _SettingsPageState();
}

class _SettingsPageState extends State<SettingsPage> {

    @override
    Widget build(BuildContext context) {
    return MyScaffold(
        appBar: MyAppBar(
            showLeading: true,
            title: "SETTINGS",
        ),
        body: Container(
            padding: EdgeInsets.symmetric(horizontal: 15),
            child: Column(
                children: SettingsConfig.setting_list.map((e) =>
                    Container(
                        child: Column(
                            children: [
                                Container(
                                    child: Row(
                                        mainAxisAlignment: MainAxisAlignment.start,
                                        children: [
                                            Text(e['label'], style: TextStyle(color: Colors.white70))
                                        ],
                                    ),
                                    margin: EdgeInsets.only(bottom: 15),
                                ),
                                Column(
                                    children: e['menu'].map<Widget>(( d){
                                        return Container(
                                          child: InkWell(
                                            child: Container(
                                                padding: EdgeInsets.symmetric(vertical: 13, horizontal: 10),
                                                decoration: BoxDecoration(
                                                    color: UI.primary_color_off,
                                                    borderRadius: BorderRadius.circular(2)
                                                ),
                                                child: Row(
                                                    children: [
                                                        d['Icon'],
                                                        Container(
                                                            margin: EdgeInsets.only(left: 15),
                                                            child: Text(d['label'], style: TextStyle(color: UI.tertiary_color_light)),
                                                        )
                                                    ],
                                                ),
                                            ),
                                            onTap: (){
                                                Navigator.pushNamed(context, d['route_name']);
                                            },
                                          ),
                                           margin: EdgeInsets.only(bottom: 13),
                                        );
                                    }).toList(),
                                )
                            ],
                        ),
                        margin: EdgeInsets.only(bottom: 30),
                    ),
                ).toList(),
            ),
        ),
    );
    }
}
