import 'package:flutter/material.dart';
import 'package:optionp2p/constants/ui.dart';
class ChangePassword extends StatefulWidget {
  @override
  _ChangePasswordState createState() => _ChangePasswordState();
}

class _ChangePasswordState extends State<ChangePassword> {


  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20),
      child: Column(
        children: [
          Container(
            padding: EdgeInsets.symmetric(vertical: 12, horizontal: 12),
            margin: EdgeInsets.only(bottom: 10),
            width: MediaQuery.of(context).size.width,
            decoration: BoxDecoration(
                color: UI.primary_color_light
            ),
            child: Text("Change Password", style: TextStyle(color: Colors.grey.shade300, fontWeight: FontWeight.w600)),
          ),
          Container(
            margin: EdgeInsets.only(top: 10, left: 20),
            child: Column(
              children: [
                Container(
                  margin: EdgeInsets.only(bottom: 12),
                  height: 35,
                  child: TextFormField(
                    decoration: InputDecoration(
                        enabledBorder: UnderlineInputBorder(
                          borderSide:BorderSide(width: 1, color: UI.primary_color_light),
                        ),
                        focusedBorder: UnderlineInputBorder(
                          borderSide:BorderSide(width: 1, color: UI.tertiary_color_light),
                        ),
                        hintText: 'New Password',
                        hintStyle: TextStyle(fontSize: 11, color: Colors.grey),
                        contentPadding: EdgeInsets.only(bottom: 18,left: 20)
                    ),
                    style: TextStyle(fontSize: 12, color: UI.tertiary_color_dark),

                  ),
                ),
                Container(
                  margin: EdgeInsets.only(bottom: 12),
                  height: 35,
                  child: TextFormField(
                    decoration: InputDecoration(
                        enabledBorder: UnderlineInputBorder(
                          borderSide:BorderSide(width: 1, color: UI.primary_color_light),
                        ),
                        focusedBorder: UnderlineInputBorder(
                          borderSide:BorderSide(width: 1, color: UI.tertiary_color_light),
                        ),
                        hintText: 'Confirm New Password',
                        hintStyle: TextStyle(fontSize: 11, color: Colors.grey),
                        contentPadding: EdgeInsets.only(bottom: 18,left: 20)
                    ),
                    style: TextStyle(fontSize: 12, color: UI.tertiary_color_dark),

                  ),
                ),
                Container(
                  margin: EdgeInsets.only(top: 20),
                  child: Row(
                    children: [
                      InkWell(
                        child: Container(
                          child: Text('Update'),
                          padding: EdgeInsets.symmetric(horizontal: 25, vertical: 10),
                          decoration: BoxDecoration(
                            color: UI.tertiary_color_blue
                          ),
                        ),
                      )
                    ],
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
