import 'package:flutter/material.dart';
import 'package:optionp2p/views/pages/settings/security/change-password.dart';
import 'package:optionp2p/views/widgets/appbar/app_bar.dart';
import 'package:optionp2p/views/widgets/layout/scaffold.dart';

class SecurityPage extends StatefulWidget {
  @override
  _SecurityPageState createState() => _SecurityPageState();
}

class _SecurityPageState extends State<SecurityPage> {
  @override
  Widget build(BuildContext context) {
    return MyScaffold(
      appBar: MyAppBar(
        title: 'SECURITY',
      ),
      body: Container(
        margin: EdgeInsets.only(top: 30),
        child: Column(
          children: [
              ChangePassword()
          ],
        ),
      ),
    );
  }
}
