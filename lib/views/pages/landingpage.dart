import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:optionp2p/constants/ui.dart';
import 'package:optionp2p/store/main-state.dart';
import 'package:optionp2p/views/pages/dashboard/index.dart';
import 'package:optionp2p/views/pages/onboarding.dart';
import 'package:optionp2p/views/widgets/components/loader.dart';
import 'package:provider/provider.dart';

class LandingPage extends StatefulWidget {
  const LandingPage({Key key}) : super(key: key);

  @override
  _LandingPageState createState() => _LandingPageState();
}

class _LandingPageState extends State<LandingPage> {
  MainState state;
  
  void initState(){
    state = Provider.of<MainState>(context, listen: false);

    Future.delayed(Duration(seconds: 1), (){
      getPref();
    });
  }
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: UI.primary_color,
      body: SafeArea(
        child: Container(
          width: MediaQuery.of(context).size.width,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
                P2pLoader(color: Colors.white)
            ],
          ),
        ),
      ),
    );
  }

  void getPref(){
    String data = state.sharedPref.getString('configurations');
    if(data != null){
      Map config = jsonDecode(data);

      Navigator.push(context, MaterialPageRoute(builder: (context)=> DashboardPage(showMenu: true)));
    }else{
      //NEW DEVICE
      Navigator.popAndPushNamed(context, '/onboarding');
    }
  }
}
