import 'package:flutter/material.dart';
import 'package:optionp2p/constants/ui.dart';
import 'package:optionp2p/services/utility.dart';
import 'package:optionp2p/views/pages/dashboard/index.dart';
import 'package:optionp2p/views/widgets/components/buttons.dart';
import 'package:optionp2p/views/widgets/layout/scaffold.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:optionp2p/services/http.dart';
import 'package:http/http.dart' as Http;

class RegisterPage extends StatefulWidget {
    @override
    _RegisterPageState createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
    bool loading = false;
    bool password_visibility = true;
    bool password_confirmation_visibility = true;

    final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey();


    TextEditingController _emailController = new TextEditingController();
    TextEditingController _passwordController = new TextEditingController();
    TextEditingController _passwordConfirmationController = new TextEditingController();

    @override
    Widget build(BuildContext context) {
        return MyScaffold(
            drawerKey: _scaffoldKey,
            body: Container(
                margin: EdgeInsets.only(top: 30),
                padding: EdgeInsets.symmetric(horizontal: 30),
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                        Container(
                            width: MediaQuery.of(context).size.width,
                            margin: EdgeInsets.only(top: 50),
                            child: Text(AppLocalizations.of(context).auth_signup_header, style: TextStyle(fontSize: 24, color: UI.tertiary_color_light, fontWeight: FontWeight.w700), textAlign: TextAlign.start),
                        ),
                        Container(
                            width: 250,
                            margin: EdgeInsets.only(top: 15),
                            child: Text(AppLocalizations.of(context).auth_signup_paragraph, style: TextStyle(color: Colors.grey, fontSize: 12), textAlign: TextAlign.start),
                        ),
                        Container(
                            margin: EdgeInsets.only(top: 70),
                            child: Column(
                                children: [
                                    Container(
                                        child: TextFormField(
                                            decoration: InputDecoration(
                                                labelText: AppLocalizations.of(context).auth_login_label,
                                                labelStyle: TextStyle(fontSize: 12, color: Colors.grey),
                                                enabledBorder: UnderlineInputBorder(
                                                    borderSide: BorderSide(color: Colors.grey.withOpacity(0.4))
                                                ),
                                                focusedBorder: UnderlineInputBorder(
                                                    borderSide: BorderSide(color: UI.tertiary_color_light)
                                                ),
                                            ),
                                            controller: _emailController,
                                        ),
                                    ),
                                    Container(
                                        child: TextFormField(
                                            decoration: InputDecoration(
                                                labelText: AppLocalizations.of(context).auth_login_password_label,
                                                labelStyle: TextStyle(fontSize: 12, color: Colors.grey),
                                                enabledBorder: UnderlineInputBorder(
                                                    borderSide: BorderSide(color: Colors.grey.withOpacity(0.4))
                                                ),
                                                focusedBorder: UnderlineInputBorder(
                                                    borderSide: BorderSide(color: UI.tertiary_color_light)
                                                ),
                                                suffixIcon: IconButton(
                                                    icon: Icon(password_visibility ? Icons.visibility_off : Icons.visibility, size: 17),
                                                    onPressed: (){
                                                        setState(() {
                                                            password_visibility = !password_visibility;
                                                        });
                                                    },
                                                )
                                            ),
                                            obscureText: password_visibility,
                                            controller: _passwordController,
                                        ),
                                    ),
                                    Container(
                                        child: TextFormField(
                                            decoration: InputDecoration(
                                                labelText: AppLocalizations.of(context).auth_login_confirm_password_label,
                                                labelStyle: TextStyle(fontSize: 12, color: Colors.grey),
                                                enabledBorder: UnderlineInputBorder(
                                                    borderSide: BorderSide(color: Colors.grey.withOpacity(0.4))
                                                ),
                                                focusedBorder: UnderlineInputBorder(
                                                    borderSide: BorderSide(color: UI.tertiary_color_light)
                                                ),
                                                suffixIcon: IconButton(
                                                    icon: Icon(password_confirmation_visibility ? Icons.visibility_off : Icons.visibility, size: 17),
                                                    onPressed: (){
                                                        setState(() {
                                                          password_confirmation_visibility = !password_confirmation_visibility;
                                                        });
                                                    },
                                                )
                                            ),
                                            obscureText: password_confirmation_visibility,
                                            controller: _passwordConfirmationController,
                                        ),
                                    )
                                ],
                            ),
                        ),
                        Container(
                            margin: EdgeInsets.only(top: 40),
                            child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                    InkWell(
                                        child: Container(
                                            child: Text(AppLocalizations.of(context).auth_signup_already_user, style: TextStyle(fontSize: 12)),
                                        ),
                                        onTap: (){
                                            Navigator.pushNamedAndRemoveUntil(context, 'login', (route) => route.settings.name == '');
                                        },
                                    ),
                                    P2pButton(
                                        text: AppLocalizations.of(context).auth_signup_button,
                                        textColor: UI.primary_color,
                                        bg: UI.tertiary_color_light,
                                        borderRadius: 5,
                                        width: UI.getWidthMediaQ(context) * 0.3,
                                        callback: createAccount,
                                        loading: loading,
                                    )
                                ],
                            ),
                        )
                    ],
                ),
            ),
        );
    }

    String validate(){
        if(_emailController.text.length == 0) return 'Email field is required';
        if(_passwordController.text.length < 6 ) return 'Password must be atleast 6 characters';
        if(_passwordConfirmationController.text.length < 6 ) return 'Confirm Password must be atleast 6 characters';

        return '';
    }

    createAccount() async{
        String v_resp = validate();
        if(v_resp.length > 0){
            Messages.snackError(context: context, message: v_resp);
            return false;
        }
        setState(()=> loading = true);

        Map<String, dynamic> payload = {
            'email': _emailController.text,
            'password': _passwordController.text,
            'password_confirmation': _passwordConfirmationController.text,
        };

        Http.Response response = await HttpService.post(
            url:HttpService.api_endpoint+"/create-account",
            payload:payload,
            context: context
        );
        setState(()=> loading = false);
        if(response.statusCode == 200){
            Messages.snackSuccess(context: context, message: "Registration was successfull");
            Navigator.push(context, MaterialPageRoute(builder: (context)=> DashboardPage()));
        }
    }


}
