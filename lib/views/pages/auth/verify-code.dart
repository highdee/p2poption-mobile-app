import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:optionp2p/constants/ui.dart';
import 'package:http/http.dart' as Http;
import 'package:optionp2p/services/http.dart';
import 'package:optionp2p/views/pages/auth/reset-password.dart';
import 'package:optionp2p/views/widgets/components/buttons.dart';
import 'package:optionp2p/views/widgets/layout/scaffold.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class VerifyCodePassword extends StatefulWidget {
  @override
  _VerifyCodePasswordState createState() => _VerifyCodePasswordState();
}

class _VerifyCodePasswordState extends State<VerifyCodePassword> {
  bool loading = false;
  bool sent = false;
  TextEditingController _codeController = new TextEditingController();

  @override
  Widget build(BuildContext context) {
    return MyScaffold(
      body: Container(
        margin: EdgeInsets.only(top: 30),
        padding: EdgeInsets.symmetric(horizontal: 30),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              width: MediaQuery.of(context).size.width,
              margin: EdgeInsets.only(top: 50),
              child: Text('Verify Reset Token', style: TextStyle(fontSize: 24, color: UI.tertiary_color_light, fontWeight: FontWeight.w700), textAlign: TextAlign.start),
            ),
            Container(
              width: 250,
              margin: EdgeInsets.only(top: 5),
              child: Text("We have sent a reset token into your email.", style: TextStyle(color: Colors.grey, fontSize: 12), textAlign: TextAlign.start),
            ),
            Container(
              margin: EdgeInsets.only(top: 50),
              child: Column(
                children: [
                  Container(
                    child: TextFormField(
                      decoration: InputDecoration(
                        labelText: 'Token',
                        labelStyle: TextStyle(fontSize: 12, color: Colors.grey),
                        enabledBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: Colors.grey.withOpacity(0.4))
                        ),
                        focusedBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: UI.tertiary_color_light)
                        ),
                      ),
                      keyboardType: TextInputType.number,
                      controller: _codeController,
                    ),
                  )
                ],
              ),
            ),
            Visibility(
              visible: _codeController.text.isNotEmpty,
              child: Container(
                margin: EdgeInsets.only(top: 20),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    P2pButton(
                      text: 'Verify',
                      textColor: UI.primary_color,
                      bg: UI.tertiary_color_light,
                      borderRadius: 5,
                      width: UI.getWidthMediaQ(context) * 0.3,
                      callback: (){
                        veirfyCode();
                      },
                      loading: loading,
                    )
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  veirfyCode() async{
    if(loading) return false;
    if(_codeController.text.length == 0) return false;
    setState(() => loading = true);

    Http.Response response = await HttpService.get(
        url:HttpService.api_endpoint+"/verify-reset-token?code="+_codeController.text,
        context: context);
    Map data = jsonDecode(response.body);
    print(data);
    if(response.statusCode == 200){
      Map data = jsonDecode(response.body);
      if(data['status']) {
        Navigator.push(context, MaterialPageRoute(builder: (context)=> ResetPasswordPage(code: _codeController.text)));
      }
    }
    setState(() => loading = false);
  }
}