import 'dart:convert';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:optionp2p/constants/ui.dart';
import 'package:optionp2p/models/User.dart';
import 'package:optionp2p/services/http.dart';
import 'package:optionp2p/services/utility.dart';
import 'package:optionp2p/views/pages/auth/forgot-password.dart';
import 'package:optionp2p/views/pages/auth/register.dart';
import 'package:optionp2p/views/pages/dashboard/index.dart';
import 'package:optionp2p/views/widgets/components/buttons.dart';
import 'package:optionp2p/views/widgets/layout/scaffold.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:http/http.dart' as Http;
import 'dart:io' show Platform;
import 'package:device_info/device_info.dart';
import 'package:provider/provider.dart';
import 'package:optionp2p/store/main-state.dart';


class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
   bool loading = false;
   bool password_visibility = true;

   TextEditingController _emailController = new TextEditingController();
   TextEditingController _passwordController = new TextEditingController();

   MainState state;

   @override
  void initState() {
    super.initState();
    state = Provider.of<MainState>(context, listen: false);

  }

  @override
  Widget build(BuildContext context) {
    return MyScaffold(
        body: Container(
            margin: EdgeInsets.only(top: 30),
            padding: EdgeInsets.symmetric(horizontal: 30),
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                  Container(
                      width: MediaQuery.of(context).size.width,
                      margin: EdgeInsets.only(top: 50),
                      child: Text(AppLocalizations.of(context).auth_login_header, style: TextStyle(fontSize: 24, color: UI.tertiary_color_light, fontWeight: FontWeight.w700), textAlign: TextAlign.start),
                  ),
                  Container(
                      width: 250,
                      margin: EdgeInsets.only(top: 15),
                      child: Text(AppLocalizations.of(context).auth_login_paragraph, style: TextStyle(color: Colors.grey, fontSize: 12), textAlign: TextAlign.start),
                  ),
                  Container(
                      margin: EdgeInsets.only(top: 70),
                      child: Column(
                        children: [
                            Container(
                                child: TextFormField(
                                    decoration: InputDecoration(
                                        labelText: AppLocalizations.of(context).auth_login_label,
                                        labelStyle: TextStyle(fontSize: 12, color: Colors.grey),
                                        enabledBorder: UnderlineInputBorder(
                                            borderSide: BorderSide(color: Colors.grey.withOpacity(0.4))
                                        ),
                                        focusedBorder: UnderlineInputBorder(
                                            borderSide: BorderSide(color: UI.tertiary_color_light)
                                        ),
                                    ),
                                    controller: _emailController,
                                ),
                            ),
                            Container(
                                child: TextFormField(
                                    decoration: InputDecoration(
                                        labelText: AppLocalizations.of(context).auth_login_password_label,
                                        labelStyle: TextStyle(fontSize: 12, color: Colors.grey),
                                        enabledBorder: UnderlineInputBorder(
                                            borderSide: BorderSide(color: Colors.grey.withOpacity(0.4))
                                        ),
                                        focusedBorder: UnderlineInputBorder(
                                            borderSide: BorderSide(color: UI.tertiary_color_light)
                                        ),
                                        suffixIcon: IconButton(
                                            icon: Icon(password_visibility ? Icons.visibility_off : Icons.visibility, size: 17),
                                            onPressed: (){
                                                setState(() {
                                                    password_visibility = !password_visibility;
                                                });
                                            },
                                        )
                                    ),
                                    obscureText: password_visibility,
                                    controller: _passwordController,
                                ),
                            )
                        ],
                    ),
                  ),
                  Container(
                      margin: EdgeInsets.only(top: 40),
                      child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                              InkWell(
                                  child: Container(
                                      child: Container(
                                          child: Text(AppLocalizations.of(context).auth_login_forgot_password, style: TextStyle(fontSize: 11)),
                                          padding: EdgeInsets.symmetric(vertical: 5),
                                      ),
                                  ),
                                  onTap: (){
                                      Navigator.push(context, MaterialPageRoute(builder: (context)=> ForgotPassword()));
                                  },
                              ),
                              P2pButton(
                                  text: AppLocalizations.of(context).auth_login_button,
                                  textColor: UI.primary_color,
                                  bg: UI.tertiary_color_light,
                                  borderRadius: 5,
                                  width: UI.getWidthMediaQ(context) * 0.3,
                                  callback: SignIn,
                                  loading: loading
                              )
                          ],
                      ),
                  ),
                  Container(
                      margin: EdgeInsets.only(top: 40),
                      child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                              InkWell(
                                  child: Text(AppLocalizations.of(context).auth_login_new_user),
                                  onTap: (){
                                      Navigator.push(context, MaterialPageRoute(builder: (context)=> RegisterPage()));
                                  },
                              )
                          ],
                      ),
                  )
              ],
          ),
        ),
    );
  }


   String validate(){
       if(_emailController.text.length == 0) return 'Email field is required';
       if(_passwordController.text.length <= 0) return 'Password field is required';

       return '';
   }

  SignIn() async{
      if(loading) return false;

      // _emailController.text = 'highdee.ai@gmail.com';
      // _passwordController.text = 'secret';

      String v_resp = validate();
      if(v_resp.length > 0){
          Messages.snackError(context: context, message: v_resp);
          return false;
      }

      setState(()=> loading = true);

      DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
      String device;
      String browser;

      if(Platform.isAndroid){
          AndroidDeviceInfo info = await deviceInfo.androidInfo;
          print(info.model);
          device = info.model;
          browser = info.device+' - '+info.version.codename;
      }else if(Platform.isIOS){
          IosDeviceInfo info = await deviceInfo.iosInfo;
          device = info.model;
          browser = info.name+' - '+info.systemVersion;
      }



      Map<String, dynamic> payload = {
          'email':_emailController.text,
          'password':_passwordController.text,
          'device':device,
          'browser':browser
      };
      Http.Response response = await HttpService.post(
          url:HttpService.api_endpoint+"/login",
          payload:payload,
          context: context);
      if(response.statusCode == 401){
          Messages.snackError(context: context, message: "Invalid username or password");
          return false;
      }
      if(response.statusCode == 200){
          Map data = jsonDecode(response.body);
          if(data['status']){
              setState(()=> loading = false);
              data['user']['token'] = data['token'];
              state.setUser(data['user']);
              state.sharedPref.setString('user', jsonEncode(data['user']));
              Navigator.push(context, MaterialPageRoute(builder: (context)=> DashboardPage(path: '/login/${data['token']}', showMenu: false,)));
              return false;
          }
      }
      setState(()=> loading = false);
  }
}
