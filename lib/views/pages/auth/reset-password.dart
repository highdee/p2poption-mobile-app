import 'package:flutter/material.dart';
import 'package:optionp2p/constants/ui.dart';
import 'package:optionp2p/services/utility.dart';
import 'package:optionp2p/views/pages/auth/login.dart';
import 'package:optionp2p/views/widgets/components/buttons.dart';
import 'package:optionp2p/views/widgets/layout/scaffold.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:optionp2p/services/http.dart';
import 'package:http/http.dart' as Http;

class ResetPasswordPage extends StatefulWidget {
    String code;
    ResetPasswordPage({
        this.code
    });
    @override
    _ResetPasswordPageState createState() => _ResetPasswordPageState();
}

class _ResetPasswordPageState extends State<ResetPasswordPage> {
    bool loading = false;
    bool password_visibility = true;
    bool password_confirmation_visibility = true;

    TextEditingController _passwordController = new TextEditingController();
    TextEditingController _passwordConfirmationController = new TextEditingController();

    @override
    Widget build(BuildContext context) {
        return MyScaffold(
            body: Container(
                margin: EdgeInsets.only(top: 30),
                padding: EdgeInsets.symmetric(horizontal: 30),
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                        Container(
                            width: MediaQuery.of(context).size.width,
                            margin: EdgeInsets.only(top: 50),
                            child: Text(AppLocalizations.of(context).auth_rp_header, style: TextStyle(fontSize: 24, color: UI.tertiary_color_light, fontWeight: FontWeight.w700), textAlign: TextAlign.start),
                        ),
                        Container(
                            width: 250,
                            margin: EdgeInsets.only(top: 15),
                            child: Text(AppLocalizations.of(context).auth_rp_paragraph, style: TextStyle(color: Colors.grey, fontSize: 12), textAlign: TextAlign.start),
                        ),
                        Container(
                            margin: EdgeInsets.only(top: 70),
                            child: Column(
                                children: [
                                    Container(
                                        child: TextFormField(
                                            decoration: InputDecoration(
                                                labelText: AppLocalizations.of(context).auth_login_new_password_label,
                                                labelStyle: TextStyle(fontSize: 12, color: Colors.grey),
                                                enabledBorder: UnderlineInputBorder(
                                                    borderSide: BorderSide(color: Colors.grey.withOpacity(0.4))
                                                ),
                                                focusedBorder: UnderlineInputBorder(
                                                    borderSide: BorderSide(color: UI.tertiary_color_light)
                                                ),
                                                suffixIcon: IconButton(
                                                    icon: Icon(password_visibility ? Icons.visibility_off : Icons.visibility, size: 17),
                                                    onPressed: (){
                                                        setState(() {
                                                            password_visibility = !password_visibility;
                                                        });
                                                    },
                                                )
                                            ),obscureText: password_visibility,
                                            controller: _passwordController,

                                        ),
                                    ),
                                    Container(
                                        child: TextFormField(
                                            decoration: InputDecoration(
                                                labelText: AppLocalizations.of(context).auth_login_confirm_password_label,
                                                labelStyle: TextStyle(fontSize: 12, color: Colors.grey),
                                                enabledBorder: UnderlineInputBorder(
                                                    borderSide: BorderSide(color: Colors.grey.withOpacity(0.4))
                                                ),
                                                focusedBorder: UnderlineInputBorder(
                                                    borderSide: BorderSide(color: UI.tertiary_color_light)
                                                ),
                                                suffixIcon: IconButton(
                                                    icon: Icon(password_confirmation_visibility ? Icons.visibility_off : Icons.visibility, size: 17),
                                                    onPressed: (){
                                                        setState(() { password_confirmation_visibility = !password_confirmation_visibility; });
                                                    },
                                                )
                                            ),obscureText: password_confirmation_visibility,
                                            controller: _passwordConfirmationController,
                                        ),
                                    )
                                ],
                            ),
                        ),
                        Container(
                            margin: EdgeInsets.only(top: 40),
                            child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                    P2pButton(
                                        text: AppLocalizations.of(context).auth_rp_button,
                                        textColor: UI.primary_color,
                                        bg: UI.tertiary_color_light,
                                        borderRadius: 5,
                                        width: UI.getWidthMediaQ(context) * 0.35,
                                        callback: createAccount,
                                        loading: loading,
                                    )
                                ],
                            ),
                        )
                    ],
                ),
            ),
        );
    }

    createAccount() async{
        String v_resp = validate();
        if(v_resp.length > 0){
            Messages.snackError(context: context, message: v_resp);
            return false;
        }
        setState(()=> loading = true);

        Map<String, dynamic> payload = {
            'token': widget.code,
            'password': _passwordController.text,
            'password_confirmation': _passwordConfirmationController.text,
        };

        Http.Response response = await HttpService.post(
            url:HttpService.api_endpoint+"/reset-password",
            payload:payload,
            context: context
        );
        setState(()=> loading = false);
        if(response.statusCode == 200){
            Messages.snackSuccess(context: context, message: "Password was reset successfully");
            Navigator.push(context, MaterialPageRoute(builder: (context)=> LoginPage()));
        }
    }

    String validate(){
        if(_passwordController.text.length < 6 ) return 'Password must be atleast 6 characters';
        if(_passwordConfirmationController.text.length < 6 ) return 'Confirm Password must be atleast 6 characters';

        return '';
    }
}
