import 'dart:convert';
import 'dart:io';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:optionp2p/constants/ui.dart';
import 'package:http/http.dart' as Http;
import 'package:optionp2p/services/http.dart';
import 'package:optionp2p/services/utility.dart';
import 'package:optionp2p/views/pages/auth/reset-password.dart';
import 'package:optionp2p/views/pages/auth/verify-code.dart';
import 'package:optionp2p/views/widgets/components/buttons.dart';
import 'package:optionp2p/views/widgets/layout/scaffold.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class ForgotPassword extends StatefulWidget {
    @override
    _ForgotPasswordState createState() => _ForgotPasswordState();
}

class _ForgotPasswordState extends State<ForgotPassword> {
    bool loading = false;
    bool sent = false;

    TextEditingController _emailController = new TextEditingController();
    TextEditingController _codeController = new TextEditingController();

    @override
    Widget build(BuildContext context) {
        return MyScaffold(
            body: Container(
                margin: EdgeInsets.only(top: 30),
                padding: EdgeInsets.symmetric(horizontal: 30),
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                        Container(
                            width: MediaQuery.of(context).size.width,
                            margin: EdgeInsets.only(top: 50),
                            child: Text(AppLocalizations.of(context).auth_fp_header, style: TextStyle(fontSize: 24, color: UI.tertiary_color_light, fontWeight: FontWeight.w700), textAlign: TextAlign.start),
                        ),
                        Container(
                            width: 250,
                            margin: EdgeInsets.only(top: 15),
                            child: Text(AppLocalizations.of(context).auth_fp_paragraph, style: TextStyle(color: Colors.grey, fontSize: 12), textAlign: TextAlign.start),
                        ),
                        Container(
                            margin: EdgeInsets.only(top: 50),
                            child: Column(
                                children: [
                                    Container(
                                        child: TextFormField(
                                            decoration: InputDecoration(
                                                labelText: AppLocalizations.of(context).auth_login_label,
                                                labelStyle: TextStyle(fontSize: 12, color: Colors.grey),
                                                enabledBorder: UnderlineInputBorder(
                                                    borderSide: BorderSide(color: Colors.grey.withOpacity(0.4))
                                                ),
                                                focusedBorder: UnderlineInputBorder(
                                                    borderSide: BorderSide(color: UI.tertiary_color_light)
                                                ),
                                            ),
                                            controller: _emailController,
                                        ),
                                    )
                                ],
                            ),
                        ),
                        Container(
                            margin: EdgeInsets.only(top: 20),
                            child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                    P2pButton(
                                        text: AppLocalizations.of(context).auth_fp_button,
                                        textColor: UI.primary_color,
                                        bg: UI.tertiary_color_light,
                                        borderRadius: 5,
                                        width: UI.getWidthMediaQ(context) * 0.3,
                                        callback: (){
                                            getCode();
                                        },
                                        loading: loading,
                                    )
                                ],
                            ),
                        )
                    ],
                ),
            ),
        );
    }

    getCode() async{
        if(loading) return false;

        setState(() => loading = true);
        Map payload = {
            'email':_emailController.text,
            'type':'code'
        };
        Http.Response response = await HttpService.post(
            url:HttpService.api_endpoint+"/forget-password",
            payload:payload,
            context: context);
        Map data = jsonDecode(response.body);
        print(data);
        if(response.statusCode == 200){
            Map data = jsonDecode(response.body);
            if(data['status']) {
                setState(() { this.sent = true;});
                Navigator.push(context, MaterialPageRoute(builder: (context)=> VerifyCodePassword()));
            }
        }
        setState(() => loading = false);
    }
}
