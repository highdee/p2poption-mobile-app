import 'dart:convert';
import 'package:flag/flag.dart';
import 'package:flutter/material.dart';
import 'package:optionp2p/constants/ui.dart';
import 'package:optionp2p/models/language.dart';
import 'package:optionp2p/views/widgets/layout/scaffold.dart';
import 'package:optionp2p/views/widgets/appbar/app_bar.dart';
import 'package:optionp2p/services/read_asset.dart';

class ChangeLanguage extends StatefulWidget {
  @override
  _ChangeLanguageState createState() => _ChangeLanguageState();
}

class _ChangeLanguageState extends State<ChangeLanguage> {

    List languages = [];

    void initState(){
      loadLanguages();
    }

  @override
  Widget build(BuildContext context) {

    print(languages);

    return MyScaffold(
      appBar: MyAppBar(
        title: "CHOOSE LANGUAGE",
        showLeading: false,
      ),
      body: Container(
          height: MediaQuery.of(context).size.height,
          child: ListView.builder(
              itemCount: languages.length,
              itemBuilder: (BuildContext context, int index){
                  Language language = Language.fromJson(languages[index]);
                  return InkWell(
                      child: Container(
                          padding: EdgeInsets.symmetric(vertical: 11, horizontal: 15),
                          margin: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
                          decoration: BoxDecoration(
                              color: UI.primary_color_light
                          ),
                          child: Row(
                              children: [
                                  language.flag,
                                  Container(
                                      margin: EdgeInsets.only(left: 20),
                                      child: Text("${language.name}", style: TextStyle(fontSize: 18),),
                                  )
                              ],
                          ),
                      ),
                      onTap: (){
                          //
                      },
                  );
              }
          ),
      )
    );
  }

  Future loadLanguages() async{
    var temp = await AssetServices.loadAsset('assets/json/languages.json');
    if(temp != null){
      setState(() { languages = jsonDecode(temp); });
    }
  }
}
