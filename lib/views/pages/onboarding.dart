import 'package:flutter/material.dart';
import 'package:optionp2p/constants/ui.dart';
import 'package:optionp2p/store/main-state.dart';
import 'package:optionp2p/views/pages/dashboard/index.dart';
import 'package:optionp2p/views/widgets/components/buttons.dart';
import 'package:optionp2p/views/widgets/layout/scaffold.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:provider/provider.dart';

class OnBoarding extends StatefulWidget {
  @override
  _OnBoardingState createState() => _OnBoardingState();
}

class _OnBoardingState extends State<OnBoarding> with SingleTickerProviderStateMixin{


  TabController _controller;
  int page = 0;

  MainState state;

  void initState(){
      super.initState();
      _controller = TabController(length: 3, vsync: this);
      state = Provider.of<MainState>(context, listen: false);
  }

  @override
  void dispose() {
      _controller.dispose();
      super.dispose();
  }



  @override
  Widget build(BuildContext context) {

    return MyScaffold(
        backgroundColors: Colors.white,
        body: Container(
            height: MediaQuery.of(context).size.height,
            child: Column(
              children: [
                  Flexible(
                      child:Column(
                          children: [
                              Flexible(
                                  flex:7,
                                  child: Container(
                                      decoration: BoxDecoration(
                                          color: UI.primary_color,
                                          borderRadius: BorderRadius.only(bottomRight: Radius.circular(60))
                                      ),
                                      child:DefaultTabController(
                                          length: 3,initialIndex: page,
                                          child: TabBarView(
                                              children: [
                                                  carouselImage('assets/images/onboarding 1.png'),
                                                  carouselImage('assets/images/onboarding 2.png'),
                                                  carouselImage('assets/images/onboarding 3.png'),
                                              ],
                                              controller: _controller,
                                              physics: NeverScrollableScrollPhysics(),
                                          ),
                                      ),
                                  ),
                              ),
                              Flexible(
                                  flex: 3,
                                  child: DefaultTabController(
                                      length: 3,
                                      child: TabBarView(
                                          children: [
                                              carouselBody(AppLocalizations.of(context).onboarding_header1, AppLocalizations.of(context).onboarding_slide1),
                                              carouselBody(AppLocalizations.of(context).onboarding_header2, AppLocalizations.of(context).onboarding_slide2),
                                              carouselBody(AppLocalizations.of(context).onboarding_header3, AppLocalizations.of(context).onboarding_slide3),
                                          ],
                                          controller: _controller,
                                          physics: NeverScrollableScrollPhysics(),
                                      ),
                                  ),
                              )
                          ],
                      ),
                  ),
                  Container(
                      margin: EdgeInsets.only(bottom: 10),
                      child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                              InkWell(
                                  child: Container(
                                      child: Text('Prev', style: TextStyle(color: page > 0 ? UI.primary_color : Colors.grey, fontWeight: FontWeight.w600)),
                                      padding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                                  ),
                                  onTap: (){
                                      if(page > 0){
                                          setState(() {page -= 1;});
                                          _controller.animateTo(page);
                                      }
                                  },
                              ),
                              Container(
                                  child: Row(
                                      children: [0,1,2].map((e) =>
                                          Container(
                                              margin: EdgeInsets.symmetric(horizontal: 4),
                                              child: Icon(Icons.stop_circle,color: e == page ? UI.primary_color:Colors.grey.withOpacity(0.4), size: 12),
                                          )
                                      ).toList(),
                                  ),
                              ),
                              InkWell(
                                  child: Container(
                                      child: Visibility(
                                          visible: page < 2,
                                          child: Text('Next', style: TextStyle(color: page < 2 ? UI.primary_color : Colors.grey, fontWeight: FontWeight.w600)),
                                          replacement: Text('Start', style: TextStyle(color: UI.tertiary_color, fontWeight: FontWeight.w600)),
                                      ),
                                      padding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                                  ),
                                  onTap: (){
                                      if(page < 2){
                                          setState(() {page += 1;});
                                          _controller.animateTo(page);
                                      }else{
                                          state.configurations['new_device'] = true;
                                          state.saveConfigurations();
                                          Navigator.push(context, MaterialPageRoute(builder: (context)=> DashboardPage()));
                                      }
                                  },
                              ),
                          ],
                      ),
                  )
              ],
          ),
        ),
    );
  }

  void slideTab(){
      print('it worked');
  }

  Widget carouselImage(String image){
      return Container(
          child: Column(
              children: [
                  Flexible(
                      child: Stack(
                          children: [
                              Container(
                                  decoration: BoxDecoration(
                                      image: DecorationImage(image: AssetImage(image), scale: 1.2),
                                  ),
                              ),
                          ],
                      ),
                  ),
              ],
          ),
      );
  }

  Widget carouselBody( String title, String bodytext){
      return Container(
          margin: EdgeInsets.only(top: 0, bottom: 0),
          child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                  Container(
                      child: Text(title, style: TextStyle(color: UI.secondary_color, fontWeight: FontWeight.w800, fontSize: 28), textAlign: TextAlign.center),
                  ),
                  Container(
                      width: 300,
                      margin: EdgeInsets.only(top: 10),
                      child: Text(bodytext, style: TextStyle(color: Colors.black87, fontSize: 13), textAlign: TextAlign.center),
                  )
              ],
          ),
      );
  }
}