import 'package:flutter/material.dart';
import 'package:optionp2p/constants/ui.dart';

class P2pTabs extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BottomNavigationBar(
        items: [
            BottomNavigationBarItem(
                icon: Icon(Icons.bar_chart),
                label: 'Trading',
                backgroundColor: UI.primary_color
            ),
            BottomNavigationBarItem(
                icon: Icon(Icons.timer_rounded),
                label: 'History',
                backgroundColor: UI.primary_color
            ),
            BottomNavigationBarItem(
                icon: Icon(Icons.wallet_membership),
                label: 'Finance',
                backgroundColor: UI.primary_color
            ),
            BottomNavigationBarItem(
                icon: Icon(Icons.menu),
                label: 'Profile',
                backgroundColor: UI.primary_color,

            ),
        ],
        onTap: (d){
            switch(d){
                case 3:
                    Navigator.pushNamed(context, 'settings');
                    break;
                default:
                    //
            }
        },
        currentIndex: 1,
        elevation: 0,
        iconSize: 26,
        backgroundColor: UI.primary_color,
        unselectedItemColor: Colors.grey.shade500,
        showSelectedLabels: false,
        selectedIconTheme: IconThemeData(
            color: UI.tertiary_color
        ),

    );
  }
}
