import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:optionp2p/constants/ui.dart';
import 'package:optionp2p/views/pages/dashboard/tabs.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:optionp2p/views/pages/menu.dart';
import 'package:optionp2p/views/widgets/components/loader.dart';

class DashboardPage extends StatefulWidget {
  String path;
  bool showMenu;


  DashboardPage({this.path='/demo-trade', this.showMenu=false});
  @override
  _DashboardPageState createState() => _DashboardPageState();
}

class _DashboardPageState extends State<DashboardPage> {

  final GlobalKey webViewKey = GlobalKey();
  InAppWebViewController webViewController;
  InAppWebViewGroupOptions options = InAppWebViewGroupOptions(
      crossPlatform: InAppWebViewOptions(
        useShouldOverrideUrlLoading: true,
        mediaPlaybackRequiresUserGesture: false,
      ),
      android: AndroidInAppWebViewOptions(
        useHybridComposition: true,
      ),
      ios: IOSInAppWebViewOptions(
        allowsInlineMediaPlayback: true,
      ));
  String url = 'https://optionp2p.io';
  bool showLoader = false;
  bool showmenu = true;

  void initState(){
    setState(() {
      showmenu = widget.showMenu;
    });

    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitDown,
      DeviceOrientation.portraitUp,
    ]);
  }

  @override
  Widget build(BuildContext context) {

    return WillPopScope(
      child: Scaffold(
          backgroundColor: UI.primary_color,
          body: SafeArea(
            child: Container(
              child: Stack(
                children: [
                  Container(
                    color: UI.primary_color,
                    child: InAppWebView(
                      key: webViewKey,
                      initialUrlRequest:
                      URLRequest(url: Uri.parse(url+widget.path)),
                      initialOptions: options,
                      onWebViewCreated: (controller) {
                        setState(() {
                          webViewController = controller;
                        });
                      },
                      onLoadStart: (controller, url){
                        setState(() {showLoader = true; });
                        HandleUrlLoading(url.path);
                      },
                      onLoadStop: (controller, url){
                        setState(() {showLoader = false; });
                      },
                    ),
                  ),

                  Visibility(
                      visible: showLoader,
                      child: Container(
                        width: MediaQuery.of(context).size.width,
                        color: UI.web_primary,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            P2pLoader(color: Colors.white)
                          ],
                        ),
                      )
                  ),
                  Visibility(
                      visible:showmenu,
                      child: Menu(navigateViewTo:navigateViewTo)
                  )
                ],
              )
            ),
          ),
          // bottomNavigationBar: P2pTabs(),
      ),
      onWillPop: (){
        if(showmenu) {
          return showDialog<void>(
            context: context,
            barrierDismissible: false,
            builder: (BuildContext context) {
              return AlertDialog(
                title: const Text('Exit App?'),
                content: SingleChildScrollView(
                  child: ListBody(
                    children: const <Widget>[
                      Text('Are you sure you want to quit the app?')
                    ],
                  ),
                ),
                actions: <Widget>[
                  TextButton(
                    child: const Text('Yes'),
                    onPressed: () {
                      exit(0);
                    },
                  ),
                  TextButton(
                    child: const Text('No'),
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                  ),
                ],
              );
            },
          );
        }
        setState(() { showmenu = true;  });
        SystemChrome.setPreferredOrientations([
          DeviceOrientation.portraitDown,
          DeviceOrientation.portraitUp,
        ]);
      },
    );
  }

  HandleUrlLoading(String path){
    print(path);
    if(path == '/login'){
      setState(() { showmenu = true;  });
      Navigator.pushNamed(context, 'login');
    }


      if(path == '/dashboard/trade-room' || path == '/demo-trade'){
        if(!showmenu) {
          SystemChrome.setPreferredOrientations([
            DeviceOrientation.landscapeLeft,
            DeviceOrientation.landscapeRight,
          ]);
        }
      }else{
        SystemChrome.setPreferredOrientations([
          DeviceOrientation.portraitDown,
          DeviceOrientation.portraitUp,
        ]);
      }
  }

  navigateViewTo(String path) async{
    path ='/'+path;
    Uri URL = await webViewController.getUrl();
    print(URL.path != path);
    if(URL.path != path){
      webViewController.loadUrl(urlRequest: URLRequest(url: Uri.parse(url+path)));
    }
    setState(() {
      showmenu = false;
    });
    HandleUrlLoading(path);
  }
}
