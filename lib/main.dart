import 'package:flutter/material.dart';
import 'package:optionp2p/store/main-state.dart';
import 'config/router.dart';
import 'dart:io';
import 'constants/ui.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';

// import 'package:path_provider/path_provider.dart';


void main() async{
    WidgetsFlutterBinding.ensureInitialized();

    //Initialising fluro routes
    MyRouter.InitialiseRouter();

    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
        statusBarColor: UI.web_primary,
        statusBarBrightness: Brightness.light,
        statusBarIconBrightness: Brightness.light,
    ));

    if (Platform.isAndroid) {
      await AndroidInAppWebViewController.setWebContentsDebuggingEnabled(true);

      var swAvailable = await AndroidWebViewFeature.isFeatureSupported(
          AndroidWebViewFeature.SERVICE_WORKER_BASIC_USAGE);
      var swInterceptAvailable = await AndroidWebViewFeature.isFeatureSupported(
          AndroidWebViewFeature.SERVICE_WORKER_SHOULD_INTERCEPT_REQUEST);

      if (swAvailable && swInterceptAvailable) {
        AndroidServiceWorkerController serviceWorkerController =
        AndroidServiceWorkerController.instance();

        serviceWorkerController.serviceWorkerClient = AndroidServiceWorkerClient(
          shouldInterceptRequest: (request) async {
            return null;
          },
        );
      }
    }

    runApp(
      MultiProvider(
          providers: [
            ChangeNotifierProvider.value(value: MainState())
          ],
          child: MaterialApp(
            initialRoute: 'landing-page',
            onGenerateRoute: MyRouter.router.generator, //Fluro routes will be generated here,
            debugShowCheckedModeBanner: false,
            theme: ThemeData(
                fontFamily: 'Montserrat',
                primaryColor: UI.primary_color,
                brightness: Brightness.dark,
                backgroundColor: UI.primary_color
            ),
            localizationsDelegates: AppLocalizations.localizationsDelegates,
            supportedLocales: AppLocalizations.supportedLocales,
          ),
      )
    );
}