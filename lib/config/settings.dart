import 'package:flutter/material.dart';
import 'package:optionp2p/constants/ui.dart';
import 'package:optionp2p/constants/ui.dart';
import 'package:optionp2p/constants/ui.dart';
import 'package:optionp2p/constants/ui.dart';
import 'package:optionp2p/constants/ui.dart';
import 'package:optionp2p/constants/ui.dart';
import 'package:optionp2p/constants/ui.dart';
import 'package:optionp2p/constants/ui.dart';

class SettingsConfig {


    static final List<Map<String, dynamic>> setting_list = [
        {
            "label" : "Profile",
            "menu"  : [
                {"label" :  "Trading Profile", "Icon"  :   Icon(Icons.multiline_chart, color: UI.tertiary_color), "route_name": "trading-profile"},
                {"label" :  "Profile", "Icon"  :   Icon(Icons.account_box, color: UI.tertiary_color), "route_name": "profile"},
                {"label" :  "Security", "Icon"  :   Icon(Icons.security, color: UI.tertiary_color), "route_name": "security"},
                {"label" :  "Trading History", "Icon"  :   Icon(Icons.av_timer, color: UI.tertiary_color), "route_name": "trading-histories"},
                {"label" :  "Identify Verification", "Icon"  :   Icon(Icons.verified_outlined, color: UI.tertiary_color), "route_name": "identity-verification"},
            ]
        },
        {
            "label" : "Support",
            "menu"  : [
                {"label" :  "Live Chat", "Icon"  :   Icon(Icons.chat, color: UI.tertiary_color), "route_name": "trading-profile"},
                {"label" :  "Video Tutorial", "Icon"  :   Icon(Icons.play_circle_outline, color: UI.tertiary_color), "route_name": "trading-profile"},
            ]
        },
        {
            "label" : "Policy",
            "menu"  : [
                {"label" :  "Terms and Condition", "Icon"  :   Icon(Icons.check_circle_outlined, color: UI.tertiary_color), "route_name": "trading-profile"},
            ]
        },
        {
            "label" : "Configurations",
            "menu"  : [
                {"label" :  "Change Language", "Icon"  :   Icon(Icons.language, color: UI.tertiary_color), "route_name": "change-language"},
            ]
        }
    ];

}

class SettingsMenu {
    String label;
    Icon icon;

    SettingsMenu({
        this.label,
        this.icon
    });
}