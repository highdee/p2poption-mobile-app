import 'package:flutter/material.dart';
import 'package:fluro/fluro.dart';

import 'package:optionp2p/config/router_handlers.dart';
import 'package:optionp2p/views/pages/change_language.dart';
import 'package:optionp2p/views/pages/auth/login.dart';
import 'package:optionp2p/views/pages/auth/register.dart';
import 'package:optionp2p/views/pages/dashboard/index.dart';
import 'package:optionp2p/views/pages/landingpage.dart';
import 'package:optionp2p/views/pages/onboarding.dart';
import 'package:optionp2p/views/pages/settings/profile/identity-verification.dart';
import 'package:optionp2p/views/pages/settings/profile/profile.dart';
import 'package:optionp2p/views/pages/settings/history/trading-histories.dart';
import 'package:optionp2p/views/pages/settings/security/security.dart';
import 'package:optionp2p/views/pages/settings/settings.dart';
import 'package:optionp2p/views/pages/settings/trading-profile.dart';

class MyRouter  {
   static FluroRouter router = FluroRouter();


   static void InitialiseRouter(){
       router.define('change-language', handler: RouterHandlers.generateHandler(ChangeLanguage()));
       router.define('home', handler: RouterHandlers.generateHandler(DashboardPage()));
       router.define('landing-page', handler: RouterHandlers.generateHandler(LandingPage()));
       router.define('onboarding', handler: RouterHandlers.generateHandler(OnBoarding()));
       router.define('login', handler: RouterHandlers.generateHandler(LoginPage()));
       router.define('register', handler: RouterHandlers.generateHandler(RegisterPage()));
       router.define('settings', handler: RouterHandlers.generateHandler(SettingsPage()));
       router.define('trading-profile', handler: RouterHandlers.generateHandler(TradingProfilePage()));
       router.define('profile', handler: RouterHandlers.generateHandler(ProfilePage()));
       router.define('security', handler: RouterHandlers.generateHandler(SecurityPage()));
       router.define('trading-histories', handler: RouterHandlers.generateHandler(TradingHistories()));
       router.define('identity-verification', handler: RouterHandlers.generateHandler(IdentityVerification()));
   }
}