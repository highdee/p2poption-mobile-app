import 'package:flutter/material.dart';
import 'package:fluro/fluro.dart';


class RouterHandlers {
    // static Handler languageHandler = Handler(handlerFunc: (BuildContext context, Map<String, List<String>> params) => ChangeLanguage());
    static Handler generateHandler(page){
        return Handler(handlerFunc: (BuildContext context, Map<String, dynamic> params) => page);
    }
}