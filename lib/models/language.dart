import 'package:flag/flag.dart';

class Language {
  String name;
  String code;
  Flag flag = Flag('');

  Language.fromJson(Map<String, dynamic> json){
    name = json['name'];
    code = json['code'];
    flag = Flag(code, width: 30, height: 30);
  }


}