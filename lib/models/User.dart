class User{
    String firstname;
    String lastname;
    String email;
    String profile_image;
    String phone;
    String date_of_birth;
    String gender;
    String country;
    String token;
    String email_verified_at;


    User.fromJson(Map<String, dynamic> data):
        firstname = data['firstname'],
        lastname = data['lastname'],
        email = data['email'],
        profile_image = data['profile_image'],
        phone = data['phone'],
        date_of_birth = data['date_of_birth'],
        gender = data['gender'],
        country = data['country'],
        email_verified_at = data['email_verified_at'],
        token = data['token'];
}