import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:optionp2p/models/User.dart';
import 'package:optionp2p/services/http.dart';
import 'package:shared_preferences/shared_preferences.dart';

class MainState with ChangeNotifier, DiagnosticableTreeMixin{
  MainState(){
    getPref().then((value) => getUser());
  }

  Map<String, dynamic> configurations={
    'new_device':true
  };

  //USER STATE
  User _User;
  User get user => _User;
  set user(User value){ _User = value;notifyListeners(); }
  void setUser(Map data){
    user = User.fromJson(data);
    HttpService.bearer_token = user.token;
    _sharedPref.setString('user', jsonEncode(data));
  }
  void getUser(){
    String data = _sharedPref.getString('user');
    if(data != null) {
      Map data_user = jsonDecode(data);
      setUser(data_user);
    }
  }

  //SHARED PREF
  SharedPreferences _sharedPref;
  SharedPreferences get sharedPref => _sharedPref;
  Future getPref() async{ _sharedPref = await SharedPreferences.getInstance();}

  void saveConfigurations(){
    _sharedPref.setString('configurations', jsonEncode(configurations));
  }
}