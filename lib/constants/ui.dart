import 'package:flutter/material.dart';

class UI {
   static const double maxScaffoldWidthAllowed = 400;
   static const Color primary_color = Color(0xfff001425);//Color(0xfff0f0f1a);
   static const Color primary_color_off = Color(0xfff00182e);//Color(0xfff131320);
   static const Color primary_color_light = Color(0xfff1a1d2d);
   static const Color secondary_color = Color(0xfff004d85);
   static const Color secondary_color_dark = Color(0xfff00264a);

   static const Color tertiary_color = Color(0xfff5897ca);
   static const Color tertiary_color_dark = Color(0xfffaeaff3);
   static const Color tertiary_color_light = Color(0xfffe9f1f7);
   static const Color tertiary_color_red = Color(0xfffff7775);
   static const Color tertiary_color_blue = Color(0xfff0986d5);

   static const Color web_primary = Color(0xfff001425);
   static const Color web_primary_off = Color(0xfff00182e);

   static double getWidthMediaQ(context){
      double width =  MediaQuery.of(context).size.width;
      return width > maxScaffoldWidthAllowed ? maxScaffoldWidthAllowed : width;
   }
}